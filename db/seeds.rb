# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Specialist.create(first_name: "Jeffrey", last_name: "Berliner", specialty: "Spinal Cord Injury")
Specialist.create(first_name: "John", last_name: "Bertini", specialty: "Urology")
Specialist.create(first_name: "Hunter", last_name: "Hammill", specialty: "OB/GYN")
Specialist.create(first_name: "Rex", last_name: "Marco", specialty: "Neurology")
Specialist.create(first_name: "Susie", last_name: "Nguyen", specialty: "Internal Medicine")

Insurance.create(name: "Aetna", street_address: "151 Farmington Avenue")
Insurance.create(name: "BlueCross BlueShield", street_address: "1310 G Street, N.W.")
Insurance.create(name: "Cigna", street_address: "900 Cottage Grove Road")
Insurance.create(name: "Medicare", street_address: "7500 Security Boulevard")
Insurance.create(name: "UnitedHealthcare", street_address: "1250 Capital of TX Hwy South")

Patient.create(first_name: "Thu-Thao", last_name: "Tran", street_address: "10118 Ripple Lake Drive", insurance_id: 4)
Patient.create(first_name: "Tina", last_name: "Vu", street_address: "10118 Ripple Lake Drive", insurance_id: 2)
Patient.create(first_name: "An", last_name: "Nguyen", street_address: "17814 Creek Bluff Lane", insurance_id: 1)
Patient.create(first_name: "Kimberly", last_name: "Vo", street_address: "12915 Dove Point Lane", insurance_id: 5)
Patient.create(first_name: "Jimmy", last_name: "Tran", street_address: "12411 Shawwood Court", insurance_id: 3)

Appointment.create(specialist_id: 1, patient_id: 1, complaint: "Low blood pressure", appointment_date: 2014-10-31, fee: 25)
Appointment.create(specialist_id: 2, patient_id: 1, complaint: "S/P tube change", appointment_date: 2014-11-15, fee: 50)
Appointment.create(specialist_id: 3, patient_id: 2, complaint: "Mammogram", appointment_date: 2014-11-30, fee: 75)
Appointment.create(specialist_id: 4, patient_id: 2, complaint: "Spine x-ray", appointment_date: 2014-12-15, fee: 100)
Appointment.create(specialist_id: 5, patient_id: 3, complaint: "Flu shot", appointment_date: 2014-12-31, fee: 125)
Appointment.create(specialist_id: 1, patient_id: 3, complaint: "Follow-up", appointment_date: 2015-01-15, fee: 150)
Appointment.create(specialist_id: 2, patient_id: 4, complaint: "Catheter change", appointment_date: 2015-01-31, fee: 175)
Appointment.create(specialist_id: 3, patient_id: 4, complaint: "Fertility treatment", appointment_date: 2015-02-15, fee: 200)
Appointment.create(specialist_id: 4, patient_id: 5, complaint: "CAT scan", appointment_date: 2015-02-28, fee: 225)
Appointment.create(specialist_id: 5, patient_id: 5, complaint: "Blood work", appointment_date: 2015-03-15, fee: 250)

