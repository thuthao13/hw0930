class Specialist < ActiveRecord::Base
  has_many :appointments
  has_many :patients, through: :appointments

  def full_name
    "Dr. #{first_name} #{last_name}"
  end
end