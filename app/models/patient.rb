class Patient < ActiveRecord::Base
  belongs_to :insurance

  has_many :appointments
  has_many :specialists, through: :appointments

  def full_name
    "#{first_name} #{last_name}"
  end
end