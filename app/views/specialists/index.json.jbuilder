json.array!(@specialists) do |specialist|
  json.extract! specialist, :id, :first_name, :last_name, :specialty
  json.url specialist_url(specialist, format: :json)
end
